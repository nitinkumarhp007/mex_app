package com.mex_app.Activites;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class JobDetailActivity extends AppCompatActivity {

    @BindView(R.id.back_arrow)
    ImageView backArrow;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.location_top)
    TextView locationTop;
    @BindView(R.id.salary)
    TextView salary;
    @BindView(R.id.job_type)
    TextView jobType;
    @BindView(R.id.message)
    Button message;
    @BindView(R.id.date_time)
    TextView dateTime;
    @BindView(R.id.skill_required)
    TextView skillRequired;
    @BindView(R.id.view_all)
    TextView viewAll;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.location)
    TextView location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobdetail);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back_arrow, R.id.message, R.id.view_all, R.id.phone, R.id.email, R.id.location})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_arrow:
                finish();
                break;
            case R.id.message:
                break;
            case R.id.view_all:
                break;
            case R.id.phone:
                break;
            case R.id.email:
                break;
            case R.id.location:
                break;
        }
    }
}
