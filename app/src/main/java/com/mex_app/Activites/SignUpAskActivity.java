package com.mex_app.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpAskActivity extends AppCompatActivity {

    @BindView(R.id.create_personal_profile)
    Button createPersonalProfile;
    @BindView(R.id.create_business_profile)
    Button createBusinessProfile;
    @BindView(R.id.signin)
    Button signin;
    @BindView(R.id.back_button)
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_ask);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.create_personal_profile, R.id.create_business_profile, R.id.signin, R.id.back_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.create_personal_profile:
                startActivity(new Intent(this, SignupActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.create_business_profile:
                startActivity(new Intent(this, SignupActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.signin:
                startActivity(new Intent(this, SigninActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;

        }
    }
}
