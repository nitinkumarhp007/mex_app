package com.mex_app.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mex_app.Activites.JobsListingActivity;
import com.mex_app.Activites.PostJobActivity;
import com.mex_app.Adapters.JobListingAdapter;
import com.mex_app.MainActivity;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobsListingFragment extends Fragment {

    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.post_ad)
    TextView postAd;

    Context context;
    Unbinder unbinder;

    public JobsListingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jobs_listing, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new JobListingAdapter(context));

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.menu, R.id.post_ad})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.menu:
                MainActivity.myDrawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.post_ad:
                startActivity(new Intent(context, PostJobActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

}
