package com.mex_app.Fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mex_app.Activites.BusninessProfilesActivity;
import com.mex_app.Adapters.BusinessCategoryAdapter;
import com.mex_app.Adapters.BusinessProfileAdapter;
import com.mex_app.MainActivity;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class BussinessProfilesFragment extends Fragment {
    Context context;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.my_recycler_view_top)
    RecyclerView myRecyclerViewTop;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.search_bar)
    TextView searchBar;
    Unbinder unbinder;

    public BussinessProfilesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bussiness_profiles, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();

        myRecyclerViewTop.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        myRecyclerViewTop.setAdapter(new BusinessCategoryAdapter(context));

        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new BusinessProfileAdapter(context));

        return view;
    }

    @OnClick(R.id.back_button)
    public void onViewClicked() {
        MainActivity.myDrawerLayout.openDrawer(Gravity.LEFT);
    }

}
