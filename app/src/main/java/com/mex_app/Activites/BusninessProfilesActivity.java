package com.mex_app.Activites;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.Adapters.BusinessCategoryAdapter;
import com.mex_app.Adapters.BusinessProfileAdapter;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BusninessProfilesActivity extends AppCompatActivity {
    BusninessProfilesActivity context;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.my_recycler_view_top)
    RecyclerView myRecyclerViewTop;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.search_bar)
    TextView searchBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busniness_profiles);
        ButterKnife.bind(this);

        context = BusninessProfilesActivity.this;

        myRecyclerViewTop.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        myRecyclerViewTop.setAdapter(new BusinessCategoryAdapter(context));

        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new BusinessProfileAdapter(context));
    }

    @OnClick(R.id.back_button)
    public void onViewClicked() {
        finish();
    }
}
