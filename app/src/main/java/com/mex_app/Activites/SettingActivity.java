package com.mex_app.Activites;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.mex_app.R;
import com.mex_app.Util.util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends AppCompatActivity {
    SettingActivity context;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.change_password)
    Button changePassword;
    @BindView(R.id.terms_condition)
    Button termsCondition;
    @BindView(R.id.privacy_policy)
    Button privacyPolicy;
    @BindView(R.id.help)
    Button help;
    @BindView(R.id.logout)
    Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        context=SettingActivity.this;
    }

    @OnClick({R.id.back_button, R.id.change_password, R.id.terms_condition, R.id.privacy_policy, R.id.help, R.id.logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.change_password:
                startActivity(new Intent(context, ChangePasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.terms_condition:
                break;
            case R.id.privacy_policy:
                break;
            case R.id.help:
                break;
            case R.id.logout:
                LogoutAlert();
                break;
        }
    }

    private void LogoutAlert() {
        new IOSDialog.Builder(this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Logout from the App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // LOGOUT_API();

                        util.showToast(SettingActivity.this, "User Logout Successfully!");
                        Intent intent = new Intent(SettingActivity.this, SigninActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
}
