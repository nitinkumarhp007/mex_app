package com.mex_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTabHost;
import androidx.fragment.app.FragmentTransaction;

import com.mex_app.Activites.BusninessProfilesActivity;
import com.mex_app.Activites.FriendsActivity;
import com.mex_app.Activites.JobsListingActivity;
import com.mex_app.Activites.PersonalProfilesActivity;
import com.mex_app.Activites.SettingActivity;
import com.mex_app.Adapters.FavoriteActivity;
import com.mex_app.Containers.Fragment_Container;
import com.mex_app.Containers.FeedContainer;
import com.mex_app.Containers.HomeContainer;
import com.mex_app.Containers.NotificationContainer;
import com.mex_app.Containers.ProfileContainer;
import com.mex_app.Fragments.BussinessProfilesFragment;
import com.mex_app.Fragments.FavoriteFragment;
import com.mex_app.Fragments.FriendsFragment;
import com.mex_app.Fragments.JobsListingFragment;
import com.mex_app.Fragments.PersonalProfilesFragment;
import com.mex_app.Util.SavePref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    public final String TAB_1_TAG = "tab_1";
    public final String TAB_2_TAG = "tab_2";
    public final String TAB_3_TAG = "tab_3";
    public final String TAB_4_TAG = "tab_4";
    public FragmentTabHost mFragmentTabHost;
    public MainActivity context;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.home)
    Button home;
    @BindView(R.id.find_a_job)
    Button findAJob;
    @BindView(R.id.find_a_business)
    Button findABusiness;
    @BindView(R.id.your_friends)
    Button yourFriends;
    @BindView(R.id.find_new_friends)
    Button findNewFriends;
    @BindView(R.id.your_favourites)
    Button yourFavourites;
    @BindView(R.id.notifications)
    Button notifications;
    @BindView(R.id.settings)
    Button settings;
    @BindView(R.id.help_support)
    Button helpSupport;
    @BindView(R.id.privacy_policy)
    Button privacyPolicy;
    @BindView(R.id.logout)
    Button logout;

    public static DrawerLayout myDrawerLayout;
    private LayoutInflater inflater = null;
    public TabWidget tabs;
    public boolean is_from_buy = false;

    public static TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        settingtabhost();
    }

    private void settingtabhost() {

        Log.e("device_token_", SavePref.getDeviceToken(this, "token"));
        is_from_buy = getIntent().getBooleanExtra("is_from_buy", false);
        context = MainActivity.this;
        title = (TextView) findViewById(R.id.title);

        myDrawerLayout = (DrawerLayout) findViewById(R.id.my_drawer_layout);

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tabs = (TabWidget) findViewById(android.R.id.tabs);
        mFragmentTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mFragmentTabHost.setup(context, getSupportFragmentManager(), R.id.realtabcontent);// getchildFragment for calling First Tab fragment from home fragment
        //inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View viewHome = prepareTabView(R.drawable.tab_background_for_home, "Home");
        TabHost.TabSpec tabSpec1 = mFragmentTabHost.newTabSpec(TAB_1_TAG).setIndicator(viewHome);
        mFragmentTabHost.addTab(tabSpec1, HomeContainer.class, null);

        View viewMessage = prepareTabView(R.drawable.tab_background_for_feed, "Feed");
        TabHost.TabSpec tabSpec2 = mFragmentTabHost.newTabSpec(TAB_2_TAG).setIndicator(viewMessage);
        mFragmentTabHost.addTab(tabSpec2, FeedContainer.class, null);

        View viewSearch = prepareTabView(R.drawable.tab_background_for_notification, "Notifications");
        TabHost.TabSpec tabSpec4 = mFragmentTabHost.newTabSpec(TAB_3_TAG).setIndicator(viewSearch);
        mFragmentTabHost.addTab(tabSpec4, NotificationContainer.class, null);

        View viewprofile = prepareTabView(R.drawable.tab_background_for_profile, "Profile");
        TabHost.TabSpec tabSpec5 = mFragmentTabHost.newTabSpec(TAB_4_TAG).setIndicator(viewprofile);
        mFragmentTabHost.addTab(tabSpec5, ProfileContainer.class, null);


        mFragmentTabHost.setCurrentTab(0);
        mFragmentTabHost.getTabWidget().setDividerDrawable(null);

        //Home. Feed. Notifications. Profile.
    }

    private View prepareTabView(int drawable, String text) {
        View view = inflater.inflate(R.layout.layout_tab, null);
        ImageView tab_image = (ImageView) view.findViewById(R.id.tab_image);
        TextView tab_text = (TextView) view.findViewById(R.id.tab_text);
        tab_text.setText(text);
        tab_image.setBackgroundResource(drawable);
        return view;
    }


    @Override
    public void onBackPressed() {
        try {
            tabs.setVisibility(View.VISIBLE);
            boolean isPopFragment = false;
            String currentTabTag = mFragmentTabHost.getCurrentTabTag();
            if (currentTabTag.equals(TAB_1_TAG)) {
                isPopFragment = ((Fragment_Container) getSupportFragmentManager().findFragmentByTag(TAB_1_TAG)).popFragment();
            } else if (currentTabTag.equals(TAB_2_TAG)) {
                isPopFragment = ((Fragment_Container) getSupportFragmentManager().findFragmentByTag(TAB_2_TAG)).popFragment();
            } else if (currentTabTag.equals(TAB_3_TAG)) {
                isPopFragment = ((Fragment_Container) getSupportFragmentManager().findFragmentByTag(TAB_3_TAG)).popFragment();
            } else if (currentTabTag.equals(TAB_4_TAG)) {
                isPopFragment = ((Fragment_Container) getSupportFragmentManager().findFragmentByTag(TAB_4_TAG)).popFragment();
            }
            if (!isPopFragment) {
                finish();
            }
        } catch (Exception ex) {
            super.onBackPressed();
        }
    }

    public void setCurrentTab(int tab_index) {
        mFragmentTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mFragmentTabHost.setCurrentTab(tab_index);
    }

    @OnClick({R.id.menu, R.id.home, R.id.find_a_job, R.id.find_a_business, R.id.your_friends, R.id.find_new_friends, R.id.your_favourites, R.id.notifications, R.id.settings, R.id.help_support, R.id.privacy_policy, R.id.logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.menu:
                myDrawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.home:
                myDrawerLayout.closeDrawer(Gravity.LEFT);
                Intent intent = new Intent(context, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.find_a_job:
                myDrawerLayout.closeDrawer(Gravity.LEFT);

                Call_fragment(new JobsListingFragment());

                //startActivity(new Intent(context, JobsListingActivity.class));
                //overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.find_a_business:
                myDrawerLayout.closeDrawer(Gravity.LEFT);
                Call_fragment(new BussinessProfilesFragment());
                //startActivity(new Intent(context, BusninessProfilesActivity.class));
                // overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.your_friends:
                myDrawerLayout.closeDrawer(Gravity.LEFT);
                Call_fragment(new FriendsFragment());
                //((Fragment_Container) getSupportFragmentManager().replaceFragment(new FavoriteFragment(), true);
                //startActivity(new Intent(context, FriendsActivity.class));
                //overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                // mFragmentTabHost.setCurrentTab(1);
                break;
            case R.id.find_new_friends:
                myDrawerLayout.closeDrawer(Gravity.LEFT);
                Call_fragment(new PersonalProfilesFragment());
                // startActivity(new Intent(context, PersonalProfilesActivity.class));
                //  overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.your_favourites:
                myDrawerLayout.closeDrawer(Gravity.LEFT);
                Call_fragment(new FavoriteFragment());
                //mFragmentTabHost.setCurrentTab(2);
                //startActivity(new Intent(context, FavoriteActivity.class));
                //overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.notifications:
                myDrawerLayout.closeDrawer(Gravity.LEFT);
                mFragmentTabHost.setCurrentTab(2);
                break;
            case R.id.settings:
                myDrawerLayout.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(context, SettingActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.help_support:
                myDrawerLayout.closeDrawer(Gravity.LEFT);
                break;
            case R.id.privacy_policy:
                myDrawerLayout.closeDrawer(Gravity.LEFT);
                break;
            case R.id.logout:
                break;
        }
    }

    private void Call_fragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_for_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
