package com.mex_app.Fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.mex_app.Activites.PersonalProfilesActivity;
import com.mex_app.Adapters.FindFriendsAdapter;
import com.mex_app.Adapters.PersonalProfilesAdapter;
import com.mex_app.MainActivity;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalProfilesFragment extends Fragment {
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Context context;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view_popular)
    RecyclerView myRecyclerViewPopular;
    @BindView(R.id.search_bar)
    EditText searchBar;

    public PersonalProfilesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal_profiles, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();

        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new PersonalProfilesAdapter(context,PersonalProfilesFragment.this));

        myRecyclerViewPopular.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        myRecyclerViewPopular.setAdapter(new FindFriendsAdapter(context));

        return view;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.menu)
    public void onViewClicked() {
        MainActivity.myDrawerLayout.openDrawer(Gravity.LEFT);
    }

}
