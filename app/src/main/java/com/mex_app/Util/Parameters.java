package com.mex_app.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "authorization_key";
    public static final String AUTH_KEY = "auth_key";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String EMAIL = "email";
    public static final String SOCIAL_ID = "social_id";
    public static final String SOCIAL_TYPE = "social_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "device_type";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String POST_ID = "post_id";
    public static final String PAYMENT_TYPE = "payment_type";
    public static final String AMOUNT = "amount";
    public static final String COMMENT = "comment";
    public static final String RATING = "rating";
    public static final String MESSAGE = "message";
    public static final String USER_ID = "receiver_id";
    public static final String MESSAGE_TYPE = "message_type";
    public static final String PROFILE = "profile";
    public static final String COUNTRY = "country";

    public static final String CARD_NO = "card_no";
    public static final String CARD_TYPE = "card_type";
    public static final String CVV = "cvv";
    public static final String EXPIRY_MONTH = "expire_month";
    public static final String EXPIRY_YEAR = "expire_year";
    public static final String PRICE = "price";
    public static final String STATUS = "status";
    public static final String PAYMENT_DETAILS = "payment_details";


}















