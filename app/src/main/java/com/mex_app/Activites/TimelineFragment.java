package com.mex_app.Activites;


import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.Adapters.PostsAdapter;
import com.mex_app.MainActivity;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimelineFragment extends Fragment {
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Context context;
    Unbinder unbinder;
    @BindView(R.id.all_posts)
    Button allPosts;
    @BindView(R.id.friends_posts)
    Button friendsPosts;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.menu)
    ImageView menu;

    public TimelineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();


        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new PostsAdapter(context));

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.menu, R.id.all_posts, R.id.friends_posts})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.all_posts:
                break;
            case R.id.menu:
                MainActivity.myDrawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.friends_posts:
                break;
        }
    }
}
