package com.mex_app.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.Fragments.FavoriteFragment;
import com.mex_app.Fragments.PersonalProfileDetailFragment;
import com.mex_app.Fragments.PersonalProfilesFragment;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FavPersonalProfilesAdapter extends RecyclerView.Adapter<FavPersonalProfilesAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    FavoriteFragment favoriteFragment;
    private View view;

    public FavPersonalProfilesAdapter(Context context, FavoriteFragment favoriteFragment) {
            this.context = context;
            this.favoriteFragment = favoriteFragment;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.fav_personal_profile_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

       /* if (list.get(position).getStatus().equals("1")) {
            holder.status.setText("Processing");
        } else if (list.get(position).getStatus().equals("2")) {
            holder.status.setText("On the way");
        } else if (list.get(position).getStatus().equals("3")) {
            holder.status.setText("Delivered");
        }


        Glide.with(context).load(list.get(position).getImage()).into(holder.image);

        holder.name.setText(list.get(position).getTitle());
        holder.price.setText("$" + list.get(position).getPrice());
        holder.sizeQuantity.setText("size:" + list.get(position).getSize() + " | Qty:" + list.get(position).getQuentity());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
*/
        if (position == 3 || position == 6) {
            holder.mainLayout.setVisibility(View.GONE);
            holder.addLayout.setVisibility(View.VISIBLE);
        } else {
            holder.addLayout.setVisibility(View.GONE);
            holder.mainLayout.setVisibility(View.VISIBLE);
        }

       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               FragmentTransaction transaction = favoriteFragment.getFragmentManager().beginTransaction();
               transaction.replace(R.id.frame_layout_for_container, new PersonalProfileDetailFragment());
               transaction.addToBackStack(null);
               transaction.commit();
           }
       });
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.main_layout)
        CardView mainLayout;
        @BindView(R.id.add_layout)
        LinearLayout addLayout;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
