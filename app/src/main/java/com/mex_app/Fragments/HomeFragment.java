package com.mex_app.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainer;
import androidx.fragment.app.FragmentTransaction;

import com.mex_app.Activites.BusninessProfilesActivity;
import com.mex_app.Activites.JobsListingActivity;
import com.mex_app.Activites.PersonalProfilesActivity;
import com.mex_app.Activites.SignUpAskActivity;
import com.mex_app.Activites.SigninActivity;
import com.mex_app.MainActivity;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    @BindView(R.id.language_)
    TextView language;
    @BindView(R.id.browse_personal_profiles)
    Button browsePersonalProfiles;
    @BindView(R.id.browse_busniness_profiles)
    Button browseBusninessProfiles;
    @BindView(R.id.employment_opportunities)
    Button employmentOpportunities;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.sign_up)
    Button signUp;
    Context context;
    Unbinder unbinder;
    @BindView(R.id.menu)
    ImageView menu;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.menu, R.id.language_, R.id.browse_personal_profiles, R.id.browse_busniness_profiles, R.id.employment_opportunities, R.id.sign_in, R.id.sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.language_:
                break;
            case R.id.menu:
                MainActivity.myDrawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.browse_personal_profiles:
                Call_fragment(new PersonalProfilesFragment());
                break;
            case R.id.browse_busniness_profiles:
                Call_fragment(new BussinessProfilesFragment());
                break;
            case R.id.employment_opportunities:
                Call_fragment(new JobsListingFragment());
                break;
            case R.id.sign_in:
                startActivity(new Intent(context, SigninActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.sign_up:
                startActivity(new Intent(context, SignUpAskActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void Call_fragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout_for_container, fragment);
        transaction.addToBackStack(null);
        // transaction.setCustomAnimations(R.anim.zoom_enter, R.anim.zoom_exit);
        transaction.commit();
    }
}
