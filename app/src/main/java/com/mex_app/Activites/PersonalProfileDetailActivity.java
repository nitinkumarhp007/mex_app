package com.mex_app.Activites;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.Adapters.FriendsDetailAdapter;
import com.mex_app.Adapters.FriendsPhotosAdapter;
import com.mex_app.Adapters.PostsAdapter;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class PersonalProfileDetailActivity extends AppCompatActivity {
    PersonalProfileDetailActivity context;

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.add_friend)
    TextView addFriend;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.view_more_info)
    TextView viewMoreInfo;
    @BindView(R.id.about)
    TextView about;
    @BindView(R.id.my_recycler_view_photos)
    RecyclerView myRecyclerViewPhotos;
    @BindView(R.id.view_all_photos)
    TextView viewAllPhotos;
    @BindView(R.id.friends_count)
    TextView friendsCount;
    @BindView(R.id.my_recycler_view_friends)
    RecyclerView myRecyclerViewFriends;
    @BindView(R.id.view_all_friends)
    TextView viewAllFriends;
    @BindView(R.id.my_recycler_view_posts)
    RecyclerView myRecyclerViewPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_profile_detail);
        ButterKnife.bind(this);

        context = PersonalProfileDetailActivity.this;

        myRecyclerViewPhotos.setLayoutManager(new GridLayoutManager(this, 3));
        myRecyclerViewPhotos.setAdapter(new FriendsPhotosAdapter(context));

        myRecyclerViewFriends.setLayoutManager(new GridLayoutManager(this, 3));
        myRecyclerViewFriends.setAdapter(new FriendsDetailAdapter(context));

        myRecyclerViewPosts.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerViewPosts.setAdapter(new PostsAdapter(context));

    }

    @OnClick({R.id.back_button, R.id.add_friend, R.id.message, R.id.view_more_info, R.id.view_all_photos, R.id.view_all_friends})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.add_friend:
                break;
            case R.id.message:
                break;
            case R.id.view_more_info:
                break;
            case R.id.view_all_photos:
                break;
            case R.id.view_all_friends:
                break;
        }
    }
}
