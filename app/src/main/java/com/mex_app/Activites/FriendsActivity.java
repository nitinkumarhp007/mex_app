package com.mex_app.Activites;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.Adapters.FriendRequetsAdapter;
import com.mex_app.Adapters.FriendsAdapter;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FriendsActivity extends AppCompatActivity {

    @BindView(R.id.friends)
    Button friends;
    @BindView(R.id.friend_requests)
    Button friendRequests;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    FriendsActivity context;
    @BindView(R.id.back_button)
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        ButterKnife.bind(this);

        context = FriendsActivity.this;

        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new FriendsAdapter(context));
    }


    @OnClick({R.id.back_button, R.id.friends, R.id.friend_requests})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.friends:
                v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                v2.setBackgroundColor(getResources().getColor(R.color.white));
                friends.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                friendRequests.setTextColor(getResources().getColor(R.color.black));
                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                myRecyclerView.setAdapter(new FriendsAdapter(context));
                break;
            case R.id.friend_requests:
                v1.setBackgroundColor(getResources().getColor(R.color.white));
                v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                friends.setTextColor(getResources().getColor(R.color.black));
                friendRequests.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                myRecyclerView.setAdapter(new FriendRequetsAdapter(context));
                break;
        }
    }
}
