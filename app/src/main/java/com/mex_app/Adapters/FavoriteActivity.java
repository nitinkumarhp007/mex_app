package com.mex_app.Adapters;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FavoriteActivity extends AppCompatActivity {
    FavoriteActivity context;
    @BindView(R.id.personal_profile)
    Button personalProfile;
    @BindView(R.id.business_profile)
    Button businessProfile;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.back_button)
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        ButterKnife.bind(this);

        context = FavoriteActivity.this;

        //myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
       // myRecyclerView.setAdapter(new PersonalProfilesAdapter(context, PersonalProfilesFragment.this));
    }


    @OnClick({R.id.back_button, R.id.personal_profile, R.id.business_profile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.personal_profile:
                v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                v2.setBackgroundColor(getResources().getColor(R.color.white));
                personalProfile.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                businessProfile.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.business_profile:
                v1.setBackgroundColor(getResources().getColor(R.color.white));
                v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                personalProfile.setTextColor(getResources().getColor(R.color.black));
                businessProfile.setTextColor(getResources().getColor(R.color.colorPrimaryDark));

                break;
        }
    }
}
