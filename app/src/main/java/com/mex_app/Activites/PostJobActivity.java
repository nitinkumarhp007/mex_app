package com.mex_app.Activites;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ligl.android.widget.iosdialog.IOSSheetDialog;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class PostJobActivity extends AppCompatActivity {

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.job_title)
    EditText jobTitle;
    @BindView(R.id.job_location)
    EditText jobLocation;
    @BindView(R.id.job_description)
    EditText jobDescription;
    @BindView(R.id.salary)
    EditText salary;
    @BindView(R.id.job_type)
    TextView jobType;
    @BindView(R.id.post)
    Button post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_job);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back_button, R.id.profile_pic, R.id.job_type, R.id.post})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.profile_pic:
                break;
            case R.id.job_type:
                IOSSheetDialog.SheetItem[] items = new IOSSheetDialog.SheetItem[2];
                items[0] = new IOSSheetDialog.SheetItem("Full Time", IOSSheetDialog.SheetItem.BLUE);
                items[1] = new IOSSheetDialog.SheetItem("Part Time", IOSSheetDialog.SheetItem.BLUE);
                IOSSheetDialog dialog2 = new IOSSheetDialog.Builder(this)
                        .setTitle("Job Type").setData(items, null).show();

                break;
            case R.id.post:
                break;
        }
    }
}
