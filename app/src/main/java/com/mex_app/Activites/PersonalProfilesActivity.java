package com.mex_app.Activites;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.Adapters.FindFriendsAdapter;
import com.mex_app.Adapters.PersonalProfilesAdapter;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PersonalProfilesActivity extends AppCompatActivity {

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    PersonalProfilesActivity context;
    @BindView(R.id.my_recycler_view_popular)
    RecyclerView myRecyclerViewPopular;
    @BindView(R.id.search_bar)
    EditText searchBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_profiles);
        ButterKnife.bind(this);

        context = PersonalProfilesActivity.this;

      //  myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
       // myRecyclerView.setAdapter(new PersonalProfilesAdapter(context, PersonalProfilesFragment.this));

        myRecyclerViewPopular.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        myRecyclerViewPopular.setAdapter(new FindFriendsAdapter(context));
    }

    @OnClick(R.id.back_button)
    public void onViewClicked() {
        finish();
    }
}
