package com.mex_app.Containers;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mex_app.Fragments.FavoriteFragment;
import com.mex_app.R;

public class FavContainer extends Fragment_Container {
    private boolean mIsViewInited=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.e("OnCreate view", "for Tab3");
        return inflater.inflate(R.layout.container_for_tab, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.e("on Activity created", "for Tab3");
        super.onActivityCreated(savedInstanceState);
        if (!mIsViewInited) {
            //mIsViewInited = true;
            onIntialView();
        }
    }

    private void onIntialView() {

        replaceFragment(new FavoriteFragment(), false);
    }
}


