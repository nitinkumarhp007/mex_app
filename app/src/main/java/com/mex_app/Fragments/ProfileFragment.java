package com.mex_app.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.Adapters.FriendsDetailAdapter;
import com.mex_app.Adapters.FriendsPhotosAdapter;
import com.mex_app.Adapters.PersonalProfilesAdapter;
import com.mex_app.Adapters.PostsAdapter;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.username)
    TextView username;
    @BindView(R.id.update_profile)
    TextView updateProfile;
    @BindView(R.id.view_more_info)
    TextView viewMoreInfo;
    @BindView(R.id.about)
    TextView about;
    @BindView(R.id.my_recycler_view_photos)
    RecyclerView myRecyclerViewPhotos;
    @BindView(R.id.view_all_photos)
    TextView viewAllPhotos;
    @BindView(R.id.friends_count)
    TextView friendsCount;
    @BindView(R.id.my_recycler_view_friends)
    RecyclerView myRecyclerViewFriends;
    @BindView(R.id.view_all_friends)
    TextView viewAllFriends;
    @BindView(R.id.my_recycler_view_posts)
    RecyclerView myRecyclerViewPosts;

    Context context;
    Unbinder unbinder;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();

        myRecyclerViewPhotos.setLayoutManager(new GridLayoutManager(context, 3));
        myRecyclerViewPhotos.setAdapter(new FriendsPhotosAdapter(context));

        myRecyclerViewFriends.setLayoutManager(new GridLayoutManager(context, 3));
        myRecyclerViewFriends.setAdapter(new FriendsDetailAdapter(context));

        myRecyclerViewPosts.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerViewPosts.setAdapter(new PostsAdapter(context));


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.update_profile, R.id.view_more_info, R.id.view_all_photos, R.id.view_all_friends})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.update_profile:
                break;
            case R.id.view_more_info:
                break;
            case R.id.view_all_photos:
                break;
            case R.id.view_all_friends:
                break;
        }
    }
}
