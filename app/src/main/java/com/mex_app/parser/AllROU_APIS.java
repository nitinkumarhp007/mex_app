package com.mex_app.parser;


public class AllROU_APIS {

    public static final String BASE_URL = "http://18.221.216.40:4000/apis/v1/";

    public static final String USERLOGIN = BASE_URL + "user/login";
    public static final String USER_SIGNUP = BASE_URL + "user/signup";
    public static final String VERIFY_OTP = BASE_URL + "user/verifiy";
    public static final String FORGOT_PASSWORD = BASE_URL + "user/forgot_password";
    public static final String LOGOUT = BASE_URL + "user/logout";
    public static final String CHANGEPASSWORD = BASE_URL + "user/change_password";
    public static final String EDIT_PROFILE = BASE_URL + "user/edit";
    public static final String HOME = BASE_URL + "home";
    public static final String POST = BASE_URL + "post";
    public static final String MYPOST = BASE_URL + "mypost";
    public static final String APP_INFO = BASE_URL + "app_information";
    public static final String FAVOURITE = BASE_URL + "favourite";

    public static final String DETAILS = BASE_URL + "post/details";
    public static final String COMMENTS = BASE_URL + "comment";
    public static final String DO_PAYMENT = BASE_URL + "do-payment";
}
