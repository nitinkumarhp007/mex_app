package com.mex_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.Activites.JobDetailActivity;
import com.mex_app.R;

import butterknife.ButterKnife;


public class JobListingAdapter extends RecyclerView.Adapter<JobListingAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;

    private View view;

    public JobListingAdapter(Context context) {
            this.context = context;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.job_list_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, JobDetailActivity.class));
            }
        });
       /* if (list.get(position).getStatus().equals("1")) {
            holder.status.setText("Processing");
        } else if (list.get(position).getStatus().equals("2")) {
            holder.status.setText("On the way");
        } else if (list.get(position).getStatus().equals("3")) {
            holder.status.setText("Delivered");
        }


        Glide.with(context).load(list.get(position).getImage()).into(holder.image);

        holder.name.setText(list.get(position).getTitle());
        holder.price.setText("$" + list.get(position).getPrice());
        holder.sizeQuantity.setText("size:" + list.get(position).getSize() + " | Qty:" + list.get(position).getQuentity());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return 7;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {


        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
