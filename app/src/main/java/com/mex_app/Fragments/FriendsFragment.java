package com.mex_app.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mex_app.Adapters.FriendRequetsAdapter;
import com.mex_app.Adapters.FriendsAdapter;
import com.mex_app.MainActivity;
import com.mex_app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {


    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    Context context;
    Unbinder unbinder;
    @BindView(R.id.friends)
    Button friends;
    @BindView(R.id.friend_requests)
    Button friendRequests;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.search_bar)
    TextView searchBar;

    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        myRecyclerView.setAdapter(new FriendsAdapter(context));
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.menu, R.id.friends, R.id.friend_requests})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.menu:
                MainActivity.myDrawerLayout.openDrawer(Gravity.LEFT);
                break;
            case R.id.friends:
                v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                v2.setBackgroundColor(getResources().getColor(R.color.white));
                friends.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                friendRequests.setTextColor(getResources().getColor(R.color.black));
                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                myRecyclerView.setAdapter(new FriendsAdapter(context));
                break;
            case R.id.friend_requests:
                v1.setBackgroundColor(getResources().getColor(R.color.white));
                v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                friends.setTextColor(getResources().getColor(R.color.black));
                friendRequests.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                myRecyclerView.setAdapter(new FriendRequetsAdapter(context));
                break;
        }
    }
}
